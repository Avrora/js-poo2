/**
 * Une classe (rien à voir avec les classes CSS) représente un modèle
 * qui permettra de créer des objets. C'est un peu comme un moule avec
 * lequel on peut créer des choses concrète (ex: si j'ai un moule à
 * gateau, je mange pas directement le moule, je fais un gateau avec
 * et c'est lui que je vais pouvoir manger)
 * On peut voir ça comme une usine à objet.
 * Un objet qu'on crée à partir d'une classe s'appelle une "instance".
 * Il peut y avoir autant d'instance d'une même classe qu'on souhaite
 * (tout comme je peux faire plein de gateaux avec un même moule et 
 * c'est tous des gateaux unique, mais qui ont la même structure)
 */

class Dog {
    /*
     * Le constructeur est une méthode spéciale des classes qui 
     * sera appelée à chaque fois qu'on fait une nouvelle instance
     * (qu'on utilise new) pour cette classe.
     * Il sert à assigner des valeurs à des propriétés de la classe 
     * (et dans le cas du JS, à définir les propriétés de celle ci)
     */
    constructor(parName, parBreed, parAge) {
        /*
        On utilise this pour faire référence à "l'instance actuelle"
        Ca signifie ici que l'instance qu'on est en train de créer aura
        une propriété name, une propriété breed et une propriété breed
        avec les valeurs qu'on a donné dans le new
        */

        this.name=parName;
        this.breed=parBreed;
        this.age=parAge;
    }
    /**
     * Une classe peut avoir des méthodes (fonction d'un objet) qui seront
     * les mêmes pour toutes les instances de cette classe.
     * Ici, on dit que toutes les instances de chien peuvent aboyer.
     */

bark() {
    alert("Maf");
}
}
