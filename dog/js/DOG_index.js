let instanceDog = new Dog("Fido", "corgi", 1);
console.log(instanceDog);
console.log(instanceDog instanceof Dog);
instanceDog.bark();

let dog = {name: "Fido", breed: "corgi", age: 1};
console.log(dog);
console.log(dog instanceof Dog);
