class Compte {
    /**
     * @param {string} nomClient 
     * @param {string} prenomClient 
     * @param {number} numeroClient 
     * @param {string} dateCreationCompte 
     * @param {string} typeCompte 
     * @param {number} solde 
     * @param {number} montant 
     */
    constructor(nomClient, prenomClient, numeroClient, dateCreationCompte, typeCompte, solde, montant) {
        this.nomClient = nomClient;
        this.prenomClient = prenomClient;
        this.numeroClient = numeroClient;
        this.dateCreationCompte = dateCreationCompte;
        this.typeCompte = typeCompte;
        this.solde = solde;    
        this.montant = montant; 
    }

    retrait(montant) { //Сумма
        this.solde = this.solde - montant; // Вычитаем сумму из solde
        return this.solde;
    }

    virement(montant, destination) { //Сумма и кому отправляем деньги
        this.solde = this.solde - montant; // Вычитаем сумму из solde
        destination.solde = destination.solde + montant; //Добавляем эту же сумму на solde первого счета
        return this.solde;
    }
}

