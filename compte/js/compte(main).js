// Создаем первый счет
let jean = new Compte ( "Demel",
                        "Jean",
                        56321,
                        "30/01/2019",
                        "Livret A",
                        1753
); 

// Создаем второй счет
let pierre = new Compte ("Brogard",
                         "Pierre",
                         342345,
                         "23/04/2018",
                         "CCP",
                         600,
                         50
);

//let phrase = `C'est le compte de {this.prenomClient}. Son solde est {this.solde}`;

let begin = document.querySelector(".begin"); //Определяем первый div для начальной суммы
let result = document.querySelector(".result"); // Определяем второй div для итоговой суммы
let phrase = document.createElement("p"); // Создаем p, который покажет первую фразу (для начальной суммы)
let phrase2 = document.createElement("p"); // Создаем p, который покажет третью фразу (для начальной суммы)
let phraseResult = document.createElement("p"); // Создаем p, который покажет вторую фразу (для итоговой суммы)
let phraseResult2 = document.createElement("p"); // Создаем p, который покажет вторую фразу (для итоговой суммы)

// Это все для первой фразы
phrase.textContent = "C'est le compte de " + jean.prenomClient + ". Son premiere solde est " + jean.solde + " euro.";
begin.appendChild(phrase);

// Это все для второй фразы (jean.retrait(25))
phraseResult.textContent = "Apres de la retrait de 25 euro, il y a " + jean.retrait(25) + " euro sur le compte de Jean.";
result.appendChild(phraseResult);
phraseResult.style.color = "red";

// Это все для третьей фразы
phrase2.textContent = "Et c'est le compte de " + pierre.prenomClient + ". Son premiere solde est " + pierre.solde + " euro.";
begin.appendChild(phrase2);
phrase2.style.color = "blue";

// Это все для четвертой фразы (pierre.virement(50, jean))
phraseResult2.textContent = "Et apres du virement de 50 euro, il y a " + pierre.virement(50, jean) + " euro sur le compte de Pierre et " + jean.solde + 
" euro sur le compte du Jean.";
result.appendChild(phraseResult2);
phraseResult2.style.color = "green";

