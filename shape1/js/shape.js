/*
I. Faire la classe et le formulaire qui crée une instance
1.  Créer un fichier shape.js dans lequel vous mettrez une classe Shape avec les propriétés suivantes: 
width:string, height:string, color:string,  roundness:number, x:number, y:number  (faire son constructor et sa JS Doc)
Essayer de faire une instance de  cette classe dans exo-shape.js et en faire un ptit console log
2. Dans le HTML, créer un formulaire bootstrap avec des inputs pour : width, height, color et roundness
3. Dans le HTML toujours, créer aussi un élément playground en dessous du formulaire, c'est là qu'on mettra nos shapes à terme
4. Dans exo-shape.js, capturer le formulaire et faire qu'au submit, on récupère les valeurs des inputs. 
Faire un console log de celles ci pour voir si ça marche bien
5. Dans l'eventListener du formulaire, utiliser les valeurs de celui ci pour créer une nouvelle instance de Shape 
et en faire un ptit console log pour voir si ça marche
Vu qu'on a pas d'input x/y, mettez les valeurs que vous voulez pour la création de l'instance

II. Faire que la Shape génère son HTML
1. Dans la classe Shape, rajouter une méthode draw(), sans argument, qui aura comme but de créer le HTML de l'instance 
(les étapes d'après disent comment faire)
2. Dans cette méthode, faire un createElement pour faire une div et la stocker dans une variable
3. En utilisant cette variable, utiliser les propriétés de l'instance pour les traduire en CSS 
(genre le x ça sera le style.left de l'élément html, la roundness ça sera le style.borderRadius etc.)
4. Faire un return de l'élément div qu'on a créé. C'est bon pour cette méthode
5. Dans le exo-shape.js, à l'intérieur de l'eventListener, utiliser l'instance qu'on a créé précédemment pour 
declencher sa méthode draw() et stocker le retour de celle ci dans une variable
6. Faire un querySelector de l'élément playground et dans l'eventListener, 
faire dessus un appendChild en lui donnant la variable de l'étape d'avant en argument

 */

 class Shape {

    constructor(width, height, color, x, y, roundness) {
        this.width = width;
        this.height = height;
        this.color = color;
        this.x = x;
        this.y = y;
        this.roundness = roundness;
    }
 }