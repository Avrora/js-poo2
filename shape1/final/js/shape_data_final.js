let form = document.querySelector("form");

form.addEventListener("submit", function(event) {
    event.preventDefault();

    // let data = new FormData(form);

    let valuesCreated = {
        width: document.querySelector("#width").value,
        height: document.querySelector("#height").value,
        color: document.querySelector("#color").value,
        roundness: document.querySelector("#roundness").value,
    };
    // console.log(valuesCreated);
    
    let shapeCreated = new Shape(valuesCreated.width,                                               
                                valuesCreated.height,
                                valuesCreated.color, 
                                valuesCreated.roundness, 
                                30, 
                                30);
    console.log(shapeCreated);
});

