/*
Petit exo classe
1. Créer un nouveau fichier user.js
2. Dans ce fichier, créer une classe User avec comme propriétés un username, un password et un email. Faire le constructor correspondant
3. Faire une méthode login dans la classe User qui attendra un username et un password en argument
4. Faire que si les arguments donnés correspondent, ça renvoie true, sinon ça renvoie false
5. Dans le fichier exo-class.js, créer deux instances de user et faire appel à leur méthode login, 
une fois avec des bonnes valeurs, une fois avec des mauvaises
6. Faire une méthode greetings() dans le classe User qui va return une chaîne de caractère avec "Hello, my name is .... 
you can contact me at ..." en remplissant les trous par le username et l'email de l'instance
*/

class User {
    constructor(username, password, email) {
        this.username=username;
        this.password=password;
        this.email=email;
    }

   greetings() {
     alert("Hello, my name is " + this.username + ". You can contact me at " + this.email);
   }

    login(username, password) {
        if (this.username === username && 
            this.password === password) {
                console.log("true");
            } else {
                console.log("false");
            }
      
    };
}